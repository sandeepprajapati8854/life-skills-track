# Life Skills

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

## Command line instructions

You can also upload existing files from your computer using the instructions below.

### Git global setup

```
git config --global user.name "Your Name"
git config --global user.email "Email ID"
```

#### Create a new repository

```
git clone https://gitlab.com/sandeepprajapati8854/life-skills-track.git
cd life-skills-track
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

#### Push an existing folder

```
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/sandeepprajapati8854/life-skills-track.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

#### Push an existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/sandeepprajapati8854/life-skills-track.git
git push -u origin --all
git push -u origin --tags
```

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to
[makeareadme](https://www.makeareadme.com/)
for this template

# Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

# Name

Choose a self-explaining name for your project.

# Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## What is firewall.

## Firewall network diagram

## Types of Firewalls

## What Firewall DO?

## Why Do We Need Firewalls?

## What is Intrusion Prevention System(IPS)

## What is Firewall Software

## How Does Firewall Software Work?

## Features of Firewall Software

## Conclusion
