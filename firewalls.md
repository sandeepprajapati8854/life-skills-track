# Firewall

In [computing,](https://en.wikipedia.org/wiki/Computing) a firewall is a network security system that monitors and controls the incoming and outgoing network traffic based on predetermined security rules. A firewall typically establishes a barrier between a trusted network and an untrusted network, such as the Internet.

### OR

A Firewall is a network security device that monitors and filters incoming and outgoing network traffic based on an organization’s previously established security policies. At its most basic, a firewall is essentially the barrier that sits between a private internal network and the public Internet. A firewall’s main purpose is to allow non-threatening traffic in and to keep dangerous traffic out.

# Firewall network diagram

The basic firewall network diagram template demonstrates how firewalls can be integrated into a network.
<br >
<br >

![Image of firewall](Images/Firewall.png)
<br >

# Types of Firewalls

- Packet filtering
- Proxy server
- Stateful inspection
- Next generation firewall

# Packet filtering

The small amount of data is analyzed and distributed according to the filter's standards.

# Proxy server

A network security system that protects while filtering messages at the application layer.

# Stateful inspection

dynamic packet filtering that monitors the active connection to determine which network packets to allow through the firewall.
<br >
<br >
![Image of statefull](Images/state.png)
<br >

# Next Generation Firewall(NGFW)

Deep packet inspection Firewall with an application-level inspection.

# What Firewall DO?

A firewall is a necessary part of any security architecture and takes the guesswork out of host-level protections and entrusts them to your network security device. Firewalls, and especially Next Generation's firewalls, focus on blocking malware and application layer attacks. These next-level firewalls can react quickly and seamlessly to detect and react to outside attacks across the whole network. They can set policies to better defend your network and carry out quick assessments to detect invasive or suspicious activity, like malware, and shut it down.

# Why Do We Need Firewalls?

Firewalls, especially Next Generation Firewalls, focus on blocking malware and application layer attacks, Along with an integrated intrusion prevention system (IPS) these Next Generation Firewalls can react quickly and seamlessly to detect and combat attacks across the whole network.

# What is Intrusion Prevention System(IPS)

Intrusion Prevention System (IPS) also known as an intrusion detection prevention system (IDPS), is a technology that keeps an eye on a network for any malicious activities attempting to exploit a known vulnerability.

# What is Firewall Software

The main two ways to deploy a firewall are firewall software running as an application on a host as a hardware firewall running on a dedicated network device. firewall software is widely used on personal and company laptops running Windows, macOS, and other Unix-like operating systems.

# How Does Firewall Software Work?

When firewall software is installed on a host, such as Windows, it can make regular network decisions down to the application level. for instance a web server application may be allowed to receive inbound connection on the standard TCP ports HTTP traffic: port 80 (HTTP) and 443 (HTTPS).

# Features of Firewall Software

- Secure
- Cost

# Conclusion

- It is clear that some form of security for private networks connected to the Internet is essential
- A firewall is an important and necessary part of security, but can not be expected to perform all the required security functions

# References

- [firewall](<https://en.wikipedia.org/wiki/Firewall_(computing)>)
- [Check point](https://www.checkpoint.com/cyber-hub/network-security/what-is-firewall/)
- [Wikipedia](https://en.wikipedia.org/wiki/Computing)
- [IPS](https://www.geeksforgeeks.org/intrusion-prevention-system-ips/)
