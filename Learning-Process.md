# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1

**What is Feynman Technique? Write your own words.**

Feynman's technique tries to explain things simply. This technique tests your level of understanding of the concept.

### Question 2

What are the different ways to implement this technique
in your learning process?

There are four key steps to the Feynman Technique:

- Choose the concept you want to learn about: Once you identify your topic, take out a blank sheet of paper. write out everything you know about the subject you want to understand as if you were teaching it to a child.

- Explain it to a 12-year-old. Now that you think you understand a topic reasonably well, explain it to a 12-year-old.

- Simplify. Only when you can explain the subject in simple terms do you understand it?

- Organize and Review. To test your understanding in the real world, run it by someone else.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Question 3

**Paraphrase the video in detail in your own words.**

This video tells us all about how to change our brains and learn new things.
Our brain functions on two modes

1. **Focus mode:**

Focus mode relies on established neural pathways and pre-existing thought patterns. A type of thinking that accesses understanding and solves immediate problems and studies concepts.

2. **Relaxed mode:**

When we are in relaxed mode our brain makes unexpected connections between ideas, rather than focusing on defined paths. As a result, it helps you create innovative solutions and connect to learn new or unfamiliar concepts with existing concepts.

### Question 4

**What are some of the steps that you can take to improve your learning process?**

I can take the following steps to improve my learning process

- I pay full attention and focus whenever I am learning a new concept.

- Take a break frequently for a short time to relax my mind.

- Do exercises on topics and revise those frequently.

- I will follow Feynman Technique to test my mind of understanding.

# 3. Learn Anything in 20 hours.

### Question 5

**Your key takeaways from the video? Paraphrase your understanding.**

**Video Summary**

we can learn anything in 20 hrs if we follow these steps :

- Learn enough to self-correct - Take any five good resources and learn from them until we can correct ourselves.
- Remove barriers in practice - During learning avoid all distractions such as televisions, smartphones, internet. Stay focused.
- Practice at least 20 hours - People get demotivated and overwhelmed at the start of learning any skill. Once we get a hold of it we are good to go.

**Takeaway**

- Avoid distractions while learning.
- We can achieve any new skills in just 20 hours.
- Stay focused while learning.
- While starting off learning new skills we might get demotivated but at least provide 20 hours to find out if you could do it.

### Question 6

**What are some of the steps that you can while approaching a new topic?**

- According to my Experience Generally, it takes 2 months to good at some skills. But it takes determination, Passion, and dedication which make me perfect.

- It is my personal experience, One must learn things early morning. Try to continue daily in life. So you will achieve better.
