# Energy Management

## 1. Manage Energy not Time

### Question 1

**What are the activities you do that make you relax - Calm quadrant?**

### Answer

Some of the activities that calm me down are:-

- Talking to friends.
- Watching my comfort shows.
- Listening to songs and shayaris.
- Fun with friends.

### Question 2

**When do you find getting into the Stress quadrant?**

### Answer

I find myself getting into the stress quadrant when I am overwhelmed and tired.

### Question 3

**How do you understand if you are in the Excitement quadrant?**

### Answer

When I am feeling joyous and happy, something good is going to happen in mylife.

## 2. Strategies to deal with Stress

## 3. Meditation

## 4. Sleep is your superpower

### Question 4

**Paraphrase the Sleep is your Superpower video in detail.**

### Answer

The video talks about the devastating impacts that lack of sleep can have on a person. Sleep deprivation can not only have a psychological but a physical toll on your body. Some of the findings of studies done on sleep deprivation are:-

- Sleep deprivation can reduce your learning capacity by 40%, as it stops the hippocampus of your brain to receive any signals.

- It was found that sleep deprivation also causes an increased risk of multiple diseases.

- A study showed that lack of sleep also negatively affects your immune system.

- Lack of sleep can also affect your DNA, making your body shun immune cells and increasing the production of cancer-producing cells.

- And your work productivity will decrease.

### Question 5

**What are some ideas that you can implement to sleep better?**

### Answer

Some of the things one can implement to get better sleep are:-

- Sleep at a regular time.

- Maintain a temperature of 18 degrees Celcius in the room you are planning to sleep.

- Forget about tommarrow plan.

## 5. Brain Changing Benefits of Exercise

### Question 6

Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

### Answer

The video talks about how exercise can change the structure of the brain and help in memory and brain function.

- Memory gets better by exercising.

- Exercising reduces the risk of cognitive diseases.

- Exercising makes your pre-frontal cortex more immune to cognitive diseases.

- Exercising can increase focus span.

### Question 7

What are some steps you can take to exercise more?

### Answer

Some of the steps you can take are:-

- Use of stairs, whenever possible

- Walking to the destination instead of a commute.

- Power-walking whenever possible.
