# Listening and Active Communication

Q1.) What are the step/strategies to do Active Listening?

Ans)

There are six key elements to Active Listening.

- Pay attention. Stay quiet and encourage the person to talk. Use non-verbal cues such as nodding, smiling, and maintaining eye contact. These cues will let the person know you are listening.

- Use encouragers. Encouragers are the sound you make, and the body movements you use when the other person is speaking. Use sounds such as, "Un-huh", or "yes". Choose a keyword from the speaker's statement and repeat it

- Don't interrupt the speaker. Don't interrupt or try to finish the speaker's sentences

- Read the body language of the speaker
- Read the body language of the speaker
- Try to feel what the speaker is feeling. If you feel sad when the person with whom you are talking expresses sadness, joyful when he expresses joy

- Provide feedback. Provide good feedback to the speaker and ask the question if you have any.

Q.2) According to Fisher's model, What are the key points of Reflective Listening?

Ans)

- Responding to the speaker's specific point, without digressing to other subjects.

- Summarized the content of the speaker.

- We should listen more than talk.

- We should respond with acceptance but not with fake concern.

Q.3) What is the obstacle in your listening process?

Ans)

- Poor listening habits. Some people may have developed bad listening habits such as interrupting.

- Lack of focus. If we are not fully present or engaged in the conversations, we may miss important information or fail to understand the speaker's message.

- Language barriers. If we are not familiar with the language being used or have difficulty understanding
  accents.

Q.4) What can you do to improve your listening process?

Ans)

Improving your listening process is an important skill that can help you build better relationships.

- Avoid distractions. Turn off your phone or other devices.

- Use body language. Body language can improve your listening process by maintaining eye contact and nodding your head.

- Show interest. Demonstrate your interest in the conversation by nodding your head, maintaining eye contact, and asking follow-up questions.

Q.5) When do you switch to a Passive communication style in your day-to-day life?

Ans)

When I am trying to avoid conflict or when I am dealing with a person who tends to be aggressive.

- When I am dealing with a difficult person. If I am dealing with a person who tends to be aggressive, passive communication can be a way to diffuse the situation and avoid escalating tensions.

- When I am not sure how the other person will react. If I am unsure how the other person will respond to what have to say, I might choose to be more passive in our communication.

Q.6) When do you switch to an aggressive communication style in your day-to-day life?

Ans)

Here is some point where I might be more likely
to use aggressive communication.

- When I am under stress. High levels of stress or frustration can lead to aggressive communication

- When I feel threatened.

Q.7) When do you switch Passive Aggressive (sarcasm/gossiping/taunt/silent treatment and others) communication styles in your day-to-day life?

Ans)

- When I feel powerless
- When I am with my close friends.

Q.8) How can you make your communication assertive?

Ans)

- Use body language to emphasize your words.
- Choose the right time, one should choose the right time to bring up an issue.
- Learn to accept kind words.
