# Grit and Growth Mindset

## 1. Grit

### Question 1

**Paraphrase (summarize) the video in a few lines. Use your own words.**

### Answer

The video talks about the significance of grit and growth mindset. The speaker talks about the significance of grit above **IQ** and other parameters.

### Question 2

**What are your key takeaways from the video to take action on?**

### Answer

Some of the takeaways from the video are listed:-

- To succeed in life **IQ** alone is not enough and grit plays a significant role.

- Having a growth mindset in life can take your performance to the next level.

## 2. Introduction to Growth Mindset

### Question 3

**Paraphrase (summarize) the video in a few lines in your own words.**

### Answer

The video talks about the two types of mindsets, one being growth and other being fixed and lists the pros of growth mindset and the cons of fixed, mindset. The video explains why growth mindset can help grow your skills and help you improve while having a fixed mindset will stagnate your skills.

### Question 4

**What are your key takeaways from the video to take action on?**

### Answer

Some of the takeaways from the video are listed:-
1- Growth Mindset
2- Fixed Mindset

growth mindset are:-

- Put more effort to learn.
- Believe intelligence can be improved.
- View feedback as an opportunity to learn
- View other's successes as source of inspirations.

fixed mindset are:-

- Give up easily
- ignore feedback from others.
- Avoid challenges to avoid failure.
- Believe putting in effort is worthless

## 3. Understanding Internal Locus of Control

### Questions 5

**What is the Internal Locus of Control? What is the key point in the video?**

### Answer

People who develop an internal locus of control believe that they are responsible for their own success. Those with an external locus of control believe that external forces, like luck, determine their outcomes.

**Key points from the video**

- Appreciate yourself when getting success because of my hard work I got success in my life.
- The degree to which you believe you have control over your life."
- How much work you put into something is something that you have complete control over.
- Take time and complete the task but don't give up.

- Build that believes you are in control of your destiny.

## 4. How to build a Growth Mindset

### Question 6

**Paraphrase (summarize) the video in a few lines in your own words.**

### Answer

**Video Summury**

- Question on you are assumptions.
- Honur the struggle.
- Believe in your ability to figure out things.
- Develop your life curriculum.

### Question 7

**What are your key takeaways from the video to take action on?**

### Answer

**Takeaways**

- I will look at my improvements.
- I will set realistic goals.
- Identifying mindset.
- I will take review the success of others.
- Honor the struggle.

## Mindset

### Question 8

**What are one or more points that you want to take action on from the manual? (Maximum-3 point)**

- I know more efforts lead to better understanding.

- I will stay with a problem till I complete it. I will not quit the problem.
- I will understand every concept.
- I will stay focused no matter what happens.
- I will wear confidence in my body.
- I will always be honest. I will have a smile on my face when I face new challenges or meet people.
